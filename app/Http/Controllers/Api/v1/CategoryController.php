<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }
    public function insertValedCategory(Request $request)
    {
        $category = $this->categoryService->storeCategory($request->name , $request->subCategory_id , $request->icon, $request->img , $request->color);
        return $category;
    }
    public function getAllCategories()
    {
        $categories = $this->categoryService->getCategories();
        return $categories;
    }
}
