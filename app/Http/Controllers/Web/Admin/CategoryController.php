<?php

namespace App\Http\Controllers\Web\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\SendRequest;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = $this->categoryService->getCategories();
        //return $categories;
        return view('Admin.category.index' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categoryService->getCategories();
        return view('Admin.category.create' , compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SendRequest $request)
    {
        $name = $request->name;
        $subCategory_id = $request->subCategory_id;
        $icon = $request->icon;
        $color = $request->color;
        if ($request->has('img')){
            $img = $request->img;
        }else{
            $img = null;
        }

        $this->categoryService->storeCategory($name , $subCategory_id , $icon , $img , $color);
        alert()->success('دسته بندی با موفقیت به ثبت رسیده است');
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

        $subCategories = $this->categoryService->getListSubCategories($category);
        return view('Admin.category.subCategory' , compact('subCategories' , 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = $this->categoryService->getCategories();
        return view('Admin.category.edit' , compact('category' , 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(SendRequest $request, Category $category)
    {
        $name = $request->name;
        $subCategory_id = $request->subCategory_id;
        $icon = $request->icon;
        $color = $request->color;
        if ($request->has('img')){
            $img = $request->img;
        }else{
            $img = null;
        }
        $this->categoryService->updateCategory($category , $name , $subCategory_id , $icon , $img , $color);
        alert()->success('دسته بندی با موفقیت به آپدیت شده است');
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
    public function sort(Request $request)
    {
        $this->categoryService->sortCategory($request->order);
    }
    public function sortSubCategory(Request $request)
    {
        //return $request->catId;
        $this->categoryService->sortSubCategory($request->order , $request->catId);
    }
    public function deleteCat(Request $request)
    {
        $this->categoryService->deleteCat($request->id);
    }
}
