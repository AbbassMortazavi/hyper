<?php

namespace App\Services\Category;

use App\Category;
use App\Services\Category\CategoryServiceInterface;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Facades\DataTables;

class CategoryService implements CategoryServiceInterface
{
    public function __construct()
    {
        //
    }

    public function getCategories()
    {
        $categories = Category::orderBy('sort_order','ASC')->where('subCategory_id' , 0)->get();
        return $categories;
    }

    public function storeCategory($name, $subCategory_id, $icon, $img, $color)
    {
        if ($img)
        {
            $url = URL::to('/');
            $orginalName=$img->getClientOriginalName();
            $img->storeAs('public/categories', $orginalName);
            $img = $url.'/storage/categories/'.$orginalName;
        }else{
            $img = null;
        }

        if ($subCategory_id)
        {
            $subCategory = $subCategory_id;
        }else{
            $subCategory = 0;
        }
        $category = Category::create([
           'name'=>$name,
           'subCategory_id'=>$subCategory,
           'icon'=>$icon,
           'img'=>$img,
           'color'=>$color,
        ]);
        return $category;
    }

    public function updateCategory($category, $name, $subCategory_id, $icon, $img, $color)
    {
        if ($img)
        {
            $url = URL::to('/');
            $orginalName=$img->getClientOriginalName();
            $img->storeAs('public/categories', $orginalName);
            $img = $url.'/storage/categories/'.$orginalName;
        }else{
            $img = $category->img;
        }
        //dd($category->subCategory_id);
        if ($category->subCategory_id == 0)
        {
            $subCategory = 0;
        }else{
            $subCategory = $subCategory_id;
        }
        $category->update([
            'name'=>$name,
            'subCategory_id'=>$subCategory,
            'icon'=>$icon,
            'img'=>$img,
            'color'=>$color,
        ]);
    }
    public function getListSubCategories($category)
    {
        $subCategories = Category::orderBy('sort_order','ASC')->where('subCategory_id' , $category->id)->get();
        return $subCategories;
    }

    public function sortCategory($order)
    {
        $tasks = Category::where('subCategory_id' , 0)->get();

        foreach ($tasks as $task) {
            $task->timestamps = false; // To disable update_at field updation
            $id = $task->id;
            //dd($request->order);
            foreach ($order as $orderr) {
                //dd($order['position']);
                if ($orderr['id'] == $id) {
                    $task->update(['sort_order' => $orderr['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function sortSubCategory($order , $catId)
    {

        $tasks = Category::where('subCategory_id' , $catId)->get();
        //return $tasks;
        //dd($tasks);
        foreach ($tasks as $task) {

            $task->timestamps = false; // To disable update_at field updation
            $id = $task->id;
            //dd($order);
            foreach ($order as $orderr) {
                //dd($orderr['position']);
                if ($orderr['id'] == $id) {
                    //dd($orderr['position']);
                    $task->update(['sort_order' => $orderr['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function getListSubCategoriess($id)
    {
        $subCategoriess = Category::where('subCategory_id' , $id)->get();
        return $subCategoriess;
    }

    public function deleteCat($id)
    {
        $category = Category::find($id);
        $category->delete();
    }
}
