<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Hyper</title>
    <link rel="icon" href="/front/favicon.ico" type="image/x-icon">

    <!-- Core css file -->
    <link rel="stylesheet" type="text/css" href="front/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="front/assets/fonts/line-icons.css">
    <link rel="stylesheet" type="text/css" href="front/assets/css/animate.css">

    <!-- Plugins CSS file -->
    <link rel="stylesheet" type="text/css" href="front/css/slicknav.css">
    <link rel="stylesheet" type="text/css" href="front/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="front/assets/css/owl.theme.css">

    <!-- Project css with  Responsive-->
    <link rel="stylesheet" type="text/css" href="front/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="front/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css"
          integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous"/>

</head>
<body>
<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    {{--<nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">--}}
        {{--<div class="container">--}}
            {{--<div class="navbar-header">--}}
                {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"--}}
                        {{--aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">--}}
                    {{--<span class="navbar-toggler-icon"></span>--}}
                    {{--<span class="icon-menu"></span>--}}
                    {{--<span class="icon-menu"></span>--}}
                    {{--<span class="icon-menu"></span>--}}
                {{--</button>--}}
                {{--<a href="index.html" class="navbar-brand"><img src="front/assets/img/logo.png" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="collapse navbar-collapse" id="main-navbar">--}}
                {{--<ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">--}}
                    {{--<li class="nav-item active"><a class="nav-link" href="#hero-area">خانه</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" href="#services">خدمات</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" href="#feature">ویژگی</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" href="#team">تیم</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" href="#pricing">قیمت گذاری</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" href="#contact">تماس</a></li>--}}
                {{--</ul>--}}
                {{--<div class="btn-sing float-right">--}}
                    {{--<a class="btn btn-border" href="../html/page-login.html">ورود</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<ul class="mobile-menu navbar-nav">--}}
            {{--<li><a class="page-scroll" href="#hero-area">خانه</a></li>--}}
            {{--<li><a class="page-scroll" href="#services">خدمات</a></li>--}}
            {{--<li><a class="page-scroll" href="#feature">ویژگی</a></li>--}}
            {{--<li><a class="page-scroll" href="#team">تیم</a></li>--}}
            {{--<li><a class="page-scroll" href="#pricing">قیمت گذاری</a></li>--}}
            {{--<li><a class="page-scroll" href="#contact">تماس</a></li>--}}
        {{--</ul>--}}

    {{--</nav>--}}
</header>

<!-- Services Section Start -->
<section id="services" class="section-padding">
    <div class="container">
        <div class="section-header text-center wow fadeInDown" data-wow-delay="0.3s">
            <h2 class="section-title">دسته بندی </h2>
            <p> </p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    @foreach($categories as $category)

                        <div class="col-xl-3 col-lg-6">
                            <a href="{{ route('subCategories.show' , $category->id) }}">
                                <div class="card" style="background-color: {{ $category->color }}">
                                    <div class="card-statistic-3 p-4">
                                        <div class="card-icon card-icon-large">
                                            <img src="{{ $category->img }}" class="img-fluid">
                                        </div>
                                        <div class="mb-4">
                                            <div class="font-icon">
                                                <i class="fas {{ $category->icon }}"></i>
                                            </div>
                                            <h5 class="card-title mb-0">{{ $category->name }}</h5>
                                            <span class="countProduct">50 محصول</span>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Footer Section Start -->
<footer id="footer" class="footer-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                <div class="footer-logo mb-3">
                    <img src="front/assets/img/logo-light.png" alt="">
                </div>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله.</p>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <h3 class="footer-titel">شرکت</h3>
                <ul>
                    <li><a href="#">اعلامیه مطبوعاتی</a></li>
                    <li><a href="#">ماموریت</a></li>
                    <li><a href="#">استراتژی</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <h3 class="footer-titel">درباره</h3>
                <ul>
                    <li><a href="#">حرفه</a></li>
                    <li><a href="#">تیم</a></li>
                    <li><a href="#">مشتریان</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <h3 class="footer-titel">پیدا کردن ما</h3>
                <div class="social-icon">
                    <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                    <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                    <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                    <a class="linkedin" href="#"><i class="lni-linkedin-filled"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<section id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>کپی رایت © 2018 کلیه حقوق محفوظ است </p>
            </div>
        </div>
    </div>
</section>

<!-- Go to Top Link -->
<a href="#" class="back-to-top"><i class="lni-arrow-up"></i></a>

<!-- Preloader -->
<div id="preloader">
    <div class="loader">
        <img src="../front/assets/images/icon.svg" width="40" height="40" alt="Oculux">
        <p>لطفا صبر کنید...</p>
    </div>
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="front/assets/js/jquery-min.js"></script>
<script src="front/assets/js/popper.min.js"></script>
<script src="front/assets/js/bootstrap.min.js"></script>

<script src="front/assets/js/wow.js"></script>
<script src="front/assets/js/scrolling-nav.js"></script>
<script src="front/assets/js/owl.carousel.min.js"></script>
<script src="front/assets/js/jquery.nav.js"></script>
<script src="front/assets/js/jquery.easing.min.js"></script>
<script src="front/assets/js/jquery.slicknav.js"></script>
<script src="front/assets/js/particles.min.js"></script>

<script src="front/assets/js/main.js"></script>
<script src="front/assets/js/form-validator.min.js"></script>
<script src="front/assets/js/contact-form-script.min.js"></script>
<script src="front/assets/js/particlesjs.js"></script>
</body>

</html>