<!doctype html>
<html lang="en">

<head>
    <title>{{ isset($title) != "" ? $title : 'Hyper' }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
    <meta name="keywords" content="admin template, Oculux admin template, dashboard template, flat admin template, responsive admin template, web app, Light Dark version">
    <meta name="author" content="GetBootstrap, design by: puffintheme.com">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/animate-css/vivify.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/c3/c3.min.css')}}"/>

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('/css/site.min.css')}}">
    @yield('style')

</head>
<body class="theme-cyan font-iransans rtl">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<!-- Theme Setting -->
<div class="themesetting">
    <div class="card theme_color">
        <div class="header">
            <h2>رنگ قالب</h2>
        </div>
        <ul class="choose-skin list-unstyled mb-0">
            <li data-theme="green"><div class="green"></div></li>
            <li data-theme="orange"><div class="orange"></div></li>
            <li data-theme="blush"><div class="blush"></div></li>
            <li data-theme="cyan" class="active"><div class="cyan"></div></li>
            <li data-theme="indigo"><div class="indigo"></div></li>
            <li data-theme="red"><div class="red"></div></li>
        </ul>
    </div>
    <div class="card font_setting">
        <div class="header">
            <h2>تنظیمات فونت</h2>
        </div>
        <div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-yekan" type="radio"><span><i></i>فونت یکان</span></label>
            </div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-iransans" type="radio" checked><span><i></i>فونت ایران سنس</span></label>
            </div>
            <div class="fancy-radio">
                <label><input name="font" value="font-shabnam" type="radio"><span><i></i>فونت شبنم</span></label>
            </div>
        </div>
    </div>
    <div class="card setting_switch">
        <div class="header">
            <h2>تنظیمات</h2>
        </div>
        <ul class="list-group">
            <li class="list-group-item">
                نسخه روشن
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="lv-btn">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                نسخه چپ چین
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="rtl-btn">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                منوی افقی
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="hmenu-btn" >
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                نوارکناری مینی
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="mini-sidebar-btn">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
        </ul>
    </div>
    <div class="card">
        <div class="form-group">
            <label class="d-block">ترافیک این ماه <span class="float-right">77%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">بارگذاری سرور <span class="float-right">50%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
            </div>
        </div>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

    @include('Admin.section.header')

    <div id="main-content">
       @yield('content')
    </div>

</div>

<!-- Javascript -->

<script src="{{asset('/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('/assets/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{asset('/assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('/assets/bundles/c3.bundle.js')}}"></script>

<script src="{{asset('/assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('/assets/js/index3.js')}}"></script>
{{--<script src="{{asset('/assets/bundles/datatablescripts.bundle.js')}}"></script>--}}
@yield('script')
</body>

</html>
