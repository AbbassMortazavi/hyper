<nav class="navbar top-navbar">
    <div class="container-fluid">

        <div class="navbar-left">
            <div class="navbar-btn">
                <a href="index-2.html"><img src="../assets/images/icon.svg" alt="Oculux Logo"
                        class="img-fluid logo"></a>
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="notification-dot bg-green">4</span>
                    </a>
                    <ul class="dropdown-menu right_chat email vivify fadeIn">
                        <li class="header green">شما 4 ایمیل جدید دارید</li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                    <div class="media-body">
                                        <span class="name">آرش خادملو <small class="float-right text-muted">همین
                                                حالا</small></span>
                                        <span class="message">بروزرسانی گیتهاب</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <div class="avtar-pic w35 bg-indigo"><span>FC</span></div>
                                    <div class="media-body">
                                        <span class="name">آرش خادملو <small class="float-right text-muted">12 دقیقه
                                                پیش</small></span>
                                        <span class="message">پیام های جدید</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">آرش خادملو <small class="float-right text-muted">38 دقیقه
                                                پیش</small></span>
                                        <span class="message">رفع اشکال طراحی</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media mb-0">
                                    <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">آرش خادملو <small class="float-right text-muted">12 دقیقه
                                                پیش</small></span>
                                        <span class="message">رفع اشکال</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <i class="icon-bell"></i>
                        <span class="notification-dot bg-azura">4</span>
                    </a>
                    <ul class="dropdown-menu feeds_widget vivify fadeIn">
                        <li class="header blue">شما 4 اطلاعیه جدید دارید</li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                                <div class="feeds-body">
                                    <h4 class="title text-danger">شماره ثابت <small class="float-right text-muted">9:10
                                            صبح</small></h4>
                                    <small>ما همه اشکال طراحی با پاسخگو را رفع کرده ایم</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="feeds-left bg-info"><i class="fa fa-user"></i></div>
                                <div class="feeds-body">
                                    <h4 class="title text-info">کاربر جدید <small class="float-right text-muted">9:10
                                            صبح</small></h4>
                                    <small>حس خوبی دارم توپ توپم! با تشکر از تیم</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="feeds-left bg-orange"><i class="fa fa-question-circle"></i></div>
                                <div class="feeds-body">
                                    <h4 class="title text-warning">هشدار سرور <small class="float-right text-muted">9:10
                                            صبح</small></h4>
                                    <small>اتصال شما خصوصی نیست</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="feeds-left bg-green"><i class="fa fa-thumbs-o-up"></i></div>
                                <div class="feeds-body">
                                    <h4 class="title text-success">2 بازخورد جدید <small
                                            class="float-right text-muted">9:10
                                            صبح</small></h4>
                                    <small>این یک پایان هوشمند برای سایت شما خواهد بود</small>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown language-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <i class="fa fa-language"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item pt-2 pb-2" href="#"><img src="../assets/images/flag/us.svg"
                                class="w20 mr-2 rounded-circle"> انگلیسی
                            آمریکایی</a>
                        <a class="dropdown-item pt-2 pb-2" href="#"><img src="../assets/images/flag/gb.svg"
                                class="w20 mr-2 rounded-circle"> انگلیسی
                            انگلیسی</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item pt-2 pb-2" href="#"><img src="../assets/images/flag/russia.svg"
                                class="w20 mr-2 rounded-circle"> روسی</a>
                        <a class="dropdown-item pt-2 pb-2" href="#"><img src="../assets/images/flag/arabia.svg"
                                class="w20 mr-2 rounded-circle"> عربی</a>
                        <a class="dropdown-item pt-2 pb-2" href="#"><img src="../assets/images/flag/france.svg"
                                class="w20 mr-2 rounded-circle"> فرانسوی</a>
                    </div>
                </li>
                <li><a href="javascript:void(0);" class="megamenu_toggle icon-menu" title="منوی مگا">مگا</a></li>
                <li class="p_social"><a href="page-social.html" class="social icon-menu" title="اخبار">اجتماعی</a></li>
                <li class="p_news"><a href="page-news.html" class="news icon-menu" title="اخبار">اخبار</a></li>
                <li class="p_blog"><a href="page-blog.html" class="blog icon-menu" title="وبلاگ">وبلاگ</a></li>
            </ul>
        </div>

        <div class="navbar-right">
            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:void(0);" class="search_toggle icon-menu" title="نتیجه جستجو"><i
                                class="icon-magnifier"></i></a></li>
                    <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="منوی راست"><i
                                class="icon-bubbles"></i><span class="notification-dot bg-pink">2</span></a></li>
                    {{--<li><a href="{{ route('user.logout') }}" class="icon-menu"><i class="icon-power"></i></a></li>--}}
                </ul>
            </div>
        </div>
    </div>
    <div class="progress-container">
        <div class="progress-bar" id="myBar"></div>
    </div>
</nav>

<div class="search_div">
    <div class="card">
        <div class="body">
            <form id="navbar-search" class="navbar-form search-form">
                <div class="input-group mb-0">
                    <input type="text" class="form-control" placeholder="جستجو...">
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="icon-magnifier"></i></span>
                        <a href="javascript:void(0);" class="search_toggle btn btn-danger"><i
                                class="icon-close"></i></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <span>نتیجه جستجو <small class="float-right text-muted">حدود 90 نتیجه (0.47 ثانیه)</small></span>
    <div class="table-responsive">
        <table class="table table-hover table-custom spacing5">
            <tbody>
                <tr>
                    <td class="w40">
                        <span>01</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="نام آواتار"><span>SS</span></div>
                            <div class="ml-3">
                                <a href="page-invoices-detail.html" title="">آرش خادملو</a>
                                <p class="mb-0">south.shyanne@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>02</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" data-placement="top"
                                title="" alt="Avatar" class="w35 h35 rounded" data-original-title="نام آواتار">
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">zoe.baker@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>03</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="avtar-pic w35 bg-indigo" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="نام آواتار"><span>CB</span></div>
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">colinbrown@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>04</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="avtar-pic w35 bg-green" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="نام آواتار"><span>KG</span></div>
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">kevin.gill@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>05</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img src="../assets/images/xs/avatar5.jpg" data-toggle="tooltip" data-placement="top"
                                title="" alt="Avatar" class="w35 h35 rounded" data-original-title="نام آواتار">
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">Maria.gill@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>06</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img src="../assets/images/xs/avatar6.jpg" data-toggle="tooltip" data-placement="top"
                                title="" alt="Avatar" class="w35 h35 rounded" data-original-title="نام آواتار">
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">kevin.baker@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>07</span>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img src="../assets/images/xs/avatar2.jpg" data-toggle="tooltip" data-placement="top"
                                title="" alt="Avatar" class="w35 h35 rounded" data-original-title="نام آواتار">
                            <div class="ml-3">
                                <a href="javascript:void(0);" title="">آرش خادملو</a>
                                <p class="mb-0">zoe.baker@example.com</p>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="megamenu" class="megamenu particles_js">
    <a href="javascript:void(0);" class="megamenu_toggle btn btn-danger"><i class="icon-close"></i></a>
    <div class="container">
        <div class="row clearfix">
            <div class="col-12">
                <ul class="q_links">
                    <li><a href="app-inbox.html"><i class="icon-envelope"></i><span>صندوق ورودی</span></a></li>
                    <li><a href="app-chat.html"><i class="icon-bubbles"></i><span>پیام رسان</span></a></li>
                    <li><a href="app-calendar.html"><i class="icon-calendar"></i><span>رویداد</span></a></li>
                    <li><a href="page-profile.html"><i class="icon-user"></i><span>پروفایل</span></a></li>
                    <li><a href="page-invoices.html"><i class="icon-printer"></i><span>صورتحساب</span></a></li>
                    <li><a href="page-timeline.html"><i class="icon-list"></i><span>خط زمانی</span></a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card w_card3">
                    <div class="body">
                        <div class="text-center"><i class="icon-picture text-info"></i>
                            <h4 class="m-t-25 mb-0">104 تصویر</h4>
                            <p>دانلود گالری شما کامل شده است</p>
                            <a href="javascript:void(0);" class="btn btn-info btn-round">دانلود</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card w_card3">
                    <div class="body">
                        <div class="text-center"><i class="icon-diamond text-success"></i>
                            <h4 class="m-t-25 mb-0">813 نقطه</h4>
                            <p>شما کار خوبی انجام می دهید!</p>
                            <a href="javascript:void(0);" class="btn btn-success btn-round">خواندن</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card w_card3">
                    <div class="body">
                        <div class="text-center"><i class="icon-social-twitter text-primary"></i>
                            <h4 class="m-t-25 mb-0">3,756</h4>
                            <p>دنبال کنندگان جدید در توییتر</p>
                            <a href="javascript:void(0);" class="btn btn-primary btn-round">بیشتر پیدا کنید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <ul class="list-group">
                    <li class="list-group-item">
                        هر کسی یک پیام به من بفرست
                        <div class="float-right">
                            <label class="switch">
                                <input type="checkbox" checked="">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        کسی که صفحه نمایه من را ببیند
                        <div class="float-right">
                            <label class="switch">
                                <input type="checkbox" checked="">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        هر کسی نظر خود را در مورد پست من ارسال کرد
                        <div class="float-right">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>

<div id="rightbar" class="rightbar">
    <div class="body">
        <ul class="nav nav-tabs2">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Chat-one">چت</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat-list">لیست</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat-groups">گروه</a></li>
        </ul>
        <hr>
        <div class="tab-content">
            <div class="tab-pane vivify fadeIn delay-100 active" id="Chat-one">
                <div class="chat_detail">
                    <ul class="chat-widget clearfix">
                        <li class="left float-left">
                            <div class="avtar-pic w35 bg-pink"><span>KG</span></div>
                            <div class="chat-info">
                                <span class="message">سلام، جان<br>به روز رسانی در پروژه X چیست؟</span>
                            </div>
                        </li>
                        <li class="right">
                            <img src="../assets/images/xs/avatar1.jpg" class="rounded" alt="">
                            <div class="chat-info">
                                <span class="message">سلام، جان<br> تقریبا تکمیل شده است من امروز به شما یک ایمیل ارسال
                                    خواهم کرد.</span>
                            </div>
                        </li>
                        <li class="left float-left">
                            <div class="avtar-pic w35 bg-pink"><span>KG</span></div>
                            <div class="chat-info">
                                <span class="message">عالیه. شما را در شب می گیرم.</span>
                            </div>
                        </li>
                        <li class="right">
                            <img src="../assets/images/xs/avatar1.jpg" class="rounded" alt="">
                            <div class="chat-info">
                                <span class="message">مطمئنا امروز ما یک انفجار داریم.</span>
                            </div>
                        </li>
                    </ul>
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <a href="javascript:void(0);" class=""><i class="icon-camera text-warning"></i></a>
                            </span>
                        </div>
                        <textarea type="text" row="" class="form-control"
                            placeholder="اینجا را وارد کنید..."></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane vvivify fadeIn delay-100" id="Chat-list">
                <ul class="right_chat list-unstyled mb-0">
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="../assets/images/xs/avatar3.jpg" alt="">
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-orange"><span>DS</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-green"><span>SW</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">از 12 ماه مه آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">از 12 ماه مه آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-indigo"><span>FC</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-pink"><span>DS</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-info"><span>SW</span></div>
                                <div class="media-body">
                                    <span class="name">آرش خادملو</span>
                                    <span class="message">از 12 ماه مه آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane vivify fadeIn delay-100" id="Chat-groups">
                <ul class="right_chat list-unstyled mb-0">
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-cyan"><span>DT</span></div>
                                <div class="media-body">
                                    <span class="name">تیم طراحیی</span>
                                    <span class="message">آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-azura"><span>SG</span></div>
                                <div class="media-body">
                                    <span class="name">تیم طراحیی</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-orange"><span>NF</span></div>
                                <div class="media-body">
                                    <span class="name">تیم طراحیی</span>
                                    <span class="message">آنلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-indigo"><span>PL</span></div>
                                <div class="media-body">
                                    <span class="name">تیم طراحیی</span>
                                    <span class="message">از 12 ماه مه آفلاین</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">
        <a href="index-2.html"><img src="images/icon.svg" alt="Logo" class="img-fluid logo"><span>ماشین ایران</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i
                class="lnr lnr-menu icon-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account">
            <div class="user_div">
                <img src="" class="user-photo"
                    alt="User Profile Picture">
            </div>
            <div class="dropdown">
                <span>خوش آمدی،</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name"
                    data-toggle="dropdown"><strong>Hyper name</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                    <li><a href="page-profile.html"><i class="icon-user"></i>پروفایل من</a></li>
                    <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>پیام ها</a></li>
                    <li><a href="javascript:void(0);"><i class="icon-settings"></i>تنظیمات</a></li>
                    <li class="divider"></li>
                    <li><a href="page-login.html"><i class="icon-power"></i>خروج</a></li>
                </ul>
            </div>
        </div>
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="{{ request()->is('panel') ? 'active': ''}}"><a href="/panel"><i
                            class="icon-speedometer active"></i><span>پیشخوان</span></a></li>

                <li
                    class="{{ request()->is('categories') || request()->is('categories/*') ? 'active' : ''}}">
                    <a href="#myPage" class="has-arrow"><i class="icon-home"></i><span>دسته بندی</span></a>
                    <ul>
                        <li class="{{ request()->is('categories') ? 'active' : '' }}"><a
                                href="/categories"><i class="icon-rocket"></i><span>ثبت دسته بندی</span></a>
                        </li>

                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
