@extends('Admin.master')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>اضافه کردن دسته بندی جدید</h1>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>ثبت دسته بندی</h2>
                        @include('Admin.errors.error')
                    </div>
                    <div class="body">
                        <form id="basic-form" method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data" novalidate>
                            @csrf
                            <div class="form-group" style="width: 80%;">
                                <label>نام دسته بندی</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                            <div class="form-group" style="width: 80%;">
                                <label>دسته بندی</label>
                                <select id="multiselect-color2" name="subCategory_id" class="form-control">
                                    <option></option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--<div class="form-group" style="width: 80%;">--}}
                                {{--<label>ساب کتگوری</label>--}}
                                {{--<select id="multiselect-color2" name="subCategory_id" class="form-control">--}}
                                    {{--<option></option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            <div class="form-group" style="width: 80%;">
                                <label>آیکن</label>
                                <input type="text" name="icon" class="form-control" required>
                            </div>
                            <div class="form-group" style="width: 80%;">
                                <label>عکس</label>
                                <input type="file" name="img" class="form-control" required>
                            </div>
                            <div class="form-group" style="width: 80%;">
                                <label>انتخاب رنگ</label>
                                <div id="cp2" class="input-group colorpicker colorpicker-component">
                                    <input type="text" name="color" value="#00AABB" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">ثبت اطلاعات</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
    <script src="{{asset('/assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.print.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/jquery-datatable.js')}}"></script>
    <script src="{{asset('/js/sweetalert.min.js')}}"></script>
    @include('sweet::alert')

    <script>
        $('.colorpicker').colorpicker({});
    </script>
@endsection
