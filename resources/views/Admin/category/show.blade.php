@extends('Admin.master')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>رنگ</h1>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>نمایش رنگ</h2>
                    </div>
                    <div class="body">

                        <div class="form-group">
                            <label>رنگ</label>
                            <input type="text" name="color" class="form-control" value="{{ $color->name }}" required>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')

    <script src="{{asset('/assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.print.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/jquery-datatable.js')}}"></script>
    <script src="{{asset('/js/sweetalert.min.js')}}"></script>
    @include('sweet::alert')

@endsection
