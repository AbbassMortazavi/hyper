@extends('Admin.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/assets/vendor/sweetalert/sweetalert.css')}}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>رنگ</h1>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary" title="">ایجاد دسته بندی</a>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>دسته بندی</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5" id="table">
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام</th>
                                    <th>آیکن</th>
                                    <th>رنگ</th>
                                    <th>عکس</th>
                                    <th>sort</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @forelse ($categories as $index=>$value)
                                    <tr class="row1" data-id="{{ $value->id }}">
                                        <td>{{$index + 1}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->icon}}</td>
                                        <td>{{$value->color}}</td>
                                        <td>
                                            <img src="{{$value->img}}" class="img-fluid" width="100" height="100"/>
                                        </td>
                                        <td>{{$value->sort_order}}</td>
                                        <td>
                                            <button type="button" data-id="{{ $value->id }}" id="delete" class="btn btn-sm btn-danger" title="" data-toggle="tooltip" data-placement="top" data-original-title="حذف دسته بندی"><i class="icon-trash"></i></button>
                                            <a href="{{ route('categories.edit' , $value->id) }}" class="btn btn-sm btn-info" title="" data-toggle="tooltip" data-placement="top" data-original-title="ویرایش دسته بندی"><i class="icon-pencil"></i></a>
                                            <a href="{{ route('categories.show' , $value->id) }}" class="btn btn-sm btn-light" title="" data-toggle="tooltip" data-placement="top" data-original-title="نمایش دسته بندی"><i class="icon-eye"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <p>No categories</p>
                                @endforelse
                                </tbody>
                            </table>
                            <h5>Refresh <button class="btn btn-default" onclick="window.location.reload()"><b>REFRESH</b></button></h5>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    {{--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>--}}
    <script src="{{asset('/assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/buttons.print.min.js')}}"></script>
    <script src="{{asset('/assets/bundles/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{asset('/assets/bundles/jquery-datatable.js')}}"></script>
    <script src="{{asset('/js/sweetalert.min.js')}}"></script>

    @include('sweet::alert')
    <script>
        $(document).on("click", "#delete", function () {
            let id = $(this).attr('data-id');
            swal({
                title: "آیا از حذف دسته بندی مطمئن هستید؟",
                text: "بعد از حذف دسته بندی دیگر دسترسی به رنگ نخواهید داشت!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: '{{ route('deleteCat.ajax') }}',
                        data: {
                            id: id,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function (result) {
                            swal("دسته بندی مورد نظر شما با موفقیت حذف شده است!!", {
                                icon: "success",
                            });
                            window.location.reload();
                        }
                    });
                } else {
                    swal("از حذف دسته بندی منصرف شده اید");
                }
            });



        });
    </script>
    <script>
        $(function () {
           $("#table").DataTable();
            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {

                var order = [];
                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('sort') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        console.log(response);

                    }
                });

            }
        });
    </script>
@endsection
