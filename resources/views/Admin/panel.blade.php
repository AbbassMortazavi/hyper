@extends('Admin.master')

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>پیشخوان</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">اوکولوکس</a></li>
                        <li class="breadcrumb-item active" aria-current="page">ارز رمزنگاری</li>
                    </ol>
                </nav>
            </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">ایجاد کمپین</a>
                    <a href="https://www.rtl-theme.com/author/paradigmshift" class="btn btn-sm btn-success" title="راست چین"><i class="icon-basket"></i> خرید قالب</a>
                </div>
        </div>
    </div>



</div>
@endsection
@section('script')

@endsection