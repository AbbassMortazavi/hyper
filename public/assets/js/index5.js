$(function() {
"use strict";

    var chart = c3.generate({
        bindto: '#chart-Event-sale-overview ', // id of chart wrapper
        data: {
            columns: [
                // each columns data
                ['داده1', 9, 14, 18, 21, 12, 21, 23, 18, 13, 9],
                ['داده2', 15, 18, 11, 15, 17, 16, 14, 17, 16, 14]
            ],
            labels: true,
            type: 'line', // default type of chart
            colors: {
                'data1': '#e96a8d',
                'data2': '#f3aca2',
            },
            names: {
                // name of each serie
                'data1': 'Sold',
                'data2': 'Available'
            }
        },
        axis: {
            x: {
                type: 'category',
                // name of each category
                categories: ['15 فور', '15 فور', '15 فور', '15 فور', '15 فور', '15 فور', '15 فور', '15 فور', '15 فور', '15 فور']
            },
        },
        legend: {
            show: true, //hide legend
        },
        padding: {
            bottom: 20,
            top: 0
        },
    });

    var chart = c3.generate({
        bindto: '#chart-Events-Interest', // id of chart wrapper
        data: {
            columns: [
                // each columns data
                ['داده1', 70],
                ['داده2', 25],
                ['داده3', 5],
            ],
            type: 'pie', // default type of chart
            colors: {
                'data1': '#e96a8d',
                'data2': '#f3aca2',
                'data3': '#f9cdac',
            },
            names: {
                // name of each serie
                'data1': 'مرد',
                'data2': 'زن',
                'data3': 'مخصوص',
            }
        },
        axis: {
        },
        legend: {
            show: true, //hide legend
        },
        padding: {
            bottom: 20,
            top: 0
        },
    });

    var chart = c3.generate({
        bindto: '#chart-Members', // id of chart wrapper
        data: {
            columns: [
                // each columns data
                ['داده1', 11, 8, 15, 18, 19, 17],
                ['داده2', 7, 7, 5, 7, 9, 12]
            ],
            type: 'bar', // default type of chart
            groups: [
                [ 'data1', 'data2']
            ],
            colors: {
                'data1': '#467fcf', // blue
                'data2': '#f66d9b', // pink
            },
            names: {
                // name of each serie
                'data1': 'User',
                'data2': 'VIP'
            }
        },
        axis: {
            x: {
                type: 'category',
                // name of each category
                categories: ['ژان', 'فور', 'مار', 'آور', 'مه', 'ژوئن']
            },
        },
        bar: {
            width: 10
        },
        legend: {
            show: false, //hide legend
        },
        padding: {
            bottom: -20,
            top: 0,
            left: -6,
        },
    });
});
