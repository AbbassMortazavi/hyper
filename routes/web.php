<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});


Route::group(['namespace'=>'Web\Admin'] , function (){
    Route::get('panel' , 'PanelController@index')->name('admin.panel');
    Route::post('sort' , 'CategoryController@sort')->name('sort.ajax');
    Route::post('sortSubCategory' , 'CategoryController@sortSubCategory')->name('sortSubCategory.ajax');
    Route::resource('categories' , 'CategoryController');
    Route::post('deleteCat' , 'CategoryController@deleteCat')->name('deleteCat.ajax');

});
Route::group(['namespace'=>'Front'] , function (){
    Route::get('/' , 'CategoryController@index');
    //Route::get('subCat/{id}' , 'CategoryController@show')->name('subCat');
    Route::resource('subCategories' , 'CategoryController');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
